package com.devcamp.customerlistjpahibernaterestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerListJpaHibernateRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerListJpaHibernateRestApiApplication.class, args);
	}

}
