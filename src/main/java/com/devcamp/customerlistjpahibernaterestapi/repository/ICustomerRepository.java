package com.devcamp.customerlistjpahibernaterestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerlistjpahibernaterestapi.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {

}
